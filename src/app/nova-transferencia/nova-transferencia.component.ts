import { TransferenciaService } from './../service/transferencia.service';
import { Component, Output,EventEmitter } from "@angular/core";
import { Transferencia } from './../models/transferencia.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nova-transferencia',
  templateUrl: './nova-transferencia.component.html',
  styleUrls: ['./nova-transferencia.component.scss'] //posso ter mais de um arquivo css dentro do Array
})

export class NovaTransferenciaComponent{

  @Output() aoTransferir = new EventEmitter<any>();

  valor: number;
  destino: number;

  constructor(private service: TransferenciaService, private router: Router){}

  transferir(){
    console.log("Solicitada Nova Transferencia");
    const valorEmitir: Transferencia = {valor: this.valor, destino: this.destino};
    this.service.adicionarNovaTransferencia(valorEmitir).subscribe(resultado =>{
      console.log(resultado);
      this.limparCampos();
      this.router.navigateByUrl("extrato");
    },
    error => console.error(error))
    this.limparCampos();
  }

  limparCampos(){
    this.valor = 0;
    this.destino = 0;
  }
}
